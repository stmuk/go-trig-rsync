# go-trig-rsync

Simple daemon to watch a directory (eg. git repo) and trigger command (eg.
rsync).  Intended for use as a cheap backup.
