package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/davecgh/go-spew/spew"
	"github.com/rjeczalik/notify"
)

// Config reflects TOML config
type Config struct {
	Lockaddr       string
	Folders        []string
	RemotePathRoot string
}

func init() {
	f, err := os.OpenFile("server.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0600)
	if err != nil {
		panic(err)
	}
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	if os.Getenv("DEBUG") == "1" {
		//if os.Getenv("DEBUG") != "1" {
		log.SetOutput(f)
	}
}

// GetConfig looks for "config.toml" in cwd or takes from cli
func GetConfig() Config {

	var conf Config

	var fn string
	flag.StringVar(&fn, "c", "config.toml", "configuration file")
	flag.Parse()

	tomlData, err := ioutil.ReadFile(fn)
	if err != nil {
		log.Fatal(err)
	}
	if _, err := toml.Decode(string(tomlData), &conf); err != nil {
		log.Fatal(err)
	}

	return conf
}

// GetWatchers gets watchers
func GetWatchers(conf Config) []chan notify.EventInfo {

	folders := conf.Folders

	// setup watchers
	cs := make([]chan notify.EventInfo, len(folders))

	i := 0

	seen := make(map[string]bool)

	for _, dir := range folders {
		log.Println(dir)

		if seen[dir] {
			log.Printf("warning: directory \"%s\" already seen. Skipping", dir)
			continue

		}

		seen[dir] = true

		if _, err := os.Stat(dir); os.IsNotExist(err) {
			log.Printf("warning: directory \"%s\" doesn't exist. Skipping", dir)
			continue
		}

		// set watcher
		c := WatchFolder(dir)
		cs[i] = c
		i++
	}

	return cs

}

// WatchFolder sets up watchers inotify (on linux)
func WatchFolder(folderName string) chan notify.EventInfo {

	// Make the channel buffered to ensure no event is dropped. Notify will drop
	// an event if the receiver is not able to keep up the sending pace.
	c := make(chan notify.EventInfo, 1)

	// Set up a watchpoint listening on events within current working directory.
	// Dispatch each create and remove events separately to c.
	if err := notify.Watch(folderName, c, notify.Create, notify.Remove); err != nil {
		log.Print(err)
	}

	return c

}

func deltaWorker(conf Config, jobs <-chan notify.EventInfo) {
	for ei := range jobs {
		log.Printf("watcher worker on file %s\n", ei.Path())

		// TODO add sleep

		cmd := "/usr/bin/rsync --relative -av --delete " + filepath.Dir(ei.Path()) + "/ " + conf.RemotePathRoot + "/"
		fmt.Printf("cmd: %v\n", cmd)

		cmds := strings.Split(cmd, " ")

		i := 0
		for {
			out, err := exec.Command(cmds[0], cmds[1:]...).Output()
			if err != nil {
				log.Print(err)
			} else {
				fmt.Printf("out: %s\n", string(out))
				log.Println("watcher worker finished job")
				break
			}

			log.Printf("delta sleeping...\n")
			time.Sleep(2 * time.Second) // TODO backoff

			i++
			if i == 2 {
				log.Println("giving up")
				break
			}
		}
	}
}

// PollWatchers polls the watchers and blocks
func PollWatchers(conf Config, cs []chan notify.EventInfo) {

	for _, c := range cs {
		defer notify.Stop(c)
	}

	jobs := make(chan notify.EventInfo, 100)

	go deltaWorker(conf, jobs)

	// poll watchers
	for _, c := range cs {
		go func(chan notify.EventInfo) {
			for {
				ei := <-c
				spew.Dump(ei)
				jobs <- ei
			}
		}(c)
	}

	select {}

}

func main() {
	conf := GetConfig()

	if conf.Lockaddr == "" {
		panic("must define Lockaddr in \"config.toml\"")
	}

	if _, err := net.Listen("tcp", conf.Lockaddr); err != nil {
		fmt.Println("an instance was already running")
		return
	}

	chs := GetWatchers(conf)
	PollWatchers(conf, chs)
}
